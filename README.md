# Uses

Make sure to check out [uses.tech](https://uses.tech) for a list of everyone's /uses pages!

# Editor + Terminal

- [Visual Studio Code](https://code.visualstudio.com) is my current editor which.
- I use [One Dark Pro](https://marketplace.visualstudio.com/items?itemName=zhuangtongfa.Material-theme) theme in [VS Code](https://code.visualstudio.com).
- I use [FiraCode v4](https://github.com/tonsky/FiraCode) as my Programming Font.
- I use [Terminal](https://en.wikipedia.org/wiki/Terminal_(macOS)) as my primary terminal.

# Desktop Apps

- I'm currently using [Firefox Browser Developer Edition](https://www.mozilla.org/en-US/firefox/developer/) for development and for browsing.
- I use [Todoist App](https://beta.todoist.com) for managing my tasks.
- I design everything in [Adobe Illustrator](https://www.adobe.com/products/illustrator).
- I use [Spotify](https://open.spotify.com) to listem to [music](https://open.spotify.com/playlist/5Mw7JRyNWXxdYtzIu9HxZZ?si=tgVdQtURSa-5zYFOEVmAtg) and code.
- I love communities, you can catch me on [Discord](http://discord.com).
- I test all my APIs using [Postman](https://www.postman.com).
- I push everything to GitHub using [Fork](https://git-fork.com/).
- I have Telegram but dont use it often [Telegram](https://telegram.org/)
- I use Termius for connect to remote hosts [Termius](https://termius.com/)

# Visual Studio Code Extensions

- [ESLint](https://marketplace.visualstudio.com/items?itemName=dbaeumer.vscode-eslint) - Integrates ESLint JavaScript into VS Code.
- [bracket-pair-colorizer-2](https://marketplace.visualstudio.com/items?itemName=CoenraadS.bracket-pair-colorizer-2) - This extension allows matching brackets to be identified with colours.
- [material-icon-theme](https://marketplace.visualstudio.com/items?itemName=PKief.material-icon-theme) - Material Design Icons for Visual Studio Code
- [discord-vscode](https://marketplace.visualstudio.com/items?itemName=icrawl.discord-vscode) - Update your discord status with the newly added rich presence.
- [dotenv](https://marketplace.visualstudio.com/items?itemName=mikestead.dotenv) - Support for dotenv file syntax
- [markdown-all-in-one](https://marketplace.visualstudio.com/items?itemName=yzhang.markdown-all-in-one) - All you need to write Markdown 
- [material-theme](https://marketplace.visualstudio.com/items?itemName=Equinusocio.vsc-material-theme) - The most epic theme now for Visual Studio Code
- [mongodb-vscode](https://marketplace.visualstudio.com/items?itemName=mongodb.mongodb-vscode) - Connect to MongoDB and Atlas directly from your VS Code environment, navigate your databases and collections, inspect your schema and use playgrounds to prototype queries and aggregations.
- [path-autocomplete](https://marketplace.visualstudio.com/items?itemName=ionutvmi.path-autocomplete) - Provides path completion for visual studio code.
- [path-intellisense](https://marketplace.visualstudio.com/items?itemName=christian-kohler.path-intellisense) - Visual Studio Code plugin that autocompletes filenames
- [code-settings-sync](https://marketplace.visualstudio.com/items?itemName=Shan.code-settings-sync) - Synchronize Settings, Snippets, Themes, File Icons, Launch, Keybindings, Workspaces and Extensions Across Multiple Machines Using GitHub Gist.
- [wakatime](https://marketplace.visualstudio.com/items?itemName=WakaTime.vscode-wakatime) - Metrics, insights, and time tracking automatically generated from your programming activity.

# FireFox Extensions

- [GitHub Repository Size](https://github.com/Shywim/github-repo-size) - Automatically adds repository size to GitHub's repository summary.
- [Grammarly](http://grammarly.com/) - Write your best with Grammarly for Chrome.
- [Enhancer for YouTube™](https://www.mrfdev.com/enhancer-for-youtube) - Tons of features to improve your user experience on YouTube™.
- [Facebook Container](https://github.com/mozilla/contain-facebook) - Facebook Container isolates your Facebook activity from the rest of your web activity in order to prevent Facebook from tracking you outside of the Facebook website via third party cookies.
- [OctoLinker](https://octolinker.now.sh/) - Links together, what belongs together.
- [Privacy Badger](Privacy Badger automatically learns to block invisible trackers) - https://privacybadger.org/
- [Tabliss](https://tabliss.io/) - A beautiful New Tab page with many customisable backgrounds and widgets that does not require any permissions.
- [Tampermonkey](https://www.tampermonkey.net/) - The world's most popular userscript manager, with over 10 million users
- [uBlock Origin](https://github.com/gorhill/uBlock#ublock-origin) - Finally, an efficient blocker. Easy on CPU and memory.
- [Windscribe](https://windscribe.com/) - Windscribe helps you mask your physical location, circumvent censorship, and block ads and trackers on websites you use every day
- [Gesturefy](https://github.com/Robbendebiene/Gesturefy) - Navigate, operate and browse faster with mouse gestures
- 
- [Wappalyzer](https://www.wappalyzer.com/) - Identify web technologies.

# Desk Setup

- [Apple MacBook Pro](https://www.apple.com/macbook-pro-16/) as Personal Laptop & Work Laptop
- [Raspberry Pi 3 Model B](https://www.raspberrypi.org/products/raspberry-pi-3-model-b/) - Single-board computer with wireless LAN and Bluetooth connectivity
- [Xbox One S](https://www.xbox.com/en-US/consoles/xbox-one-s) - Gaming Console

# Mobile Apps

- [GitHub Mobile](https://github.com/mobile/) - The world’s development platform, in your pocket.
- [Spotify](https://www.spotify.com/us/free/) - Spotify is a digital music service that gives you access to millions of songs.
- [Todoist](https://apps.apple.com/us/app/todoist-to-do-list-tasks/id572688855) - The to do list to organize work & life.
- [Discord](https://apps.apple.com/us/app/discord/id985746746) - Chat for Communities and Friends.
- [Termius](https://termius.com/) - Termius is the SSH client that works on Desktop and Mobile
- [Authy](https://authy.com/) - Two-factor Authentication App
